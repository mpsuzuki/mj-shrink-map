#!/usr/bin/env ruby
require "./ruby-utils/getOpts.rb"
require "./ruby-utils/rotator.rb"
require "set"
require "json"

rot = Rotator.new(10000)
mapDict = Hash.new()

STDERR.printf(".")
while (STDIN.gets())
  STDERR.printf(rot.get())
  toks = $_.gsub(/#.*/, "").chomp().split("\t")
  next if (toks.length < 3)
  uhex, tag, dictIndice, = toks
  next if (tag == "kKangXi" && dictIndice[-1..-1] == "1")

  ucsInt = uhex[2..-1].hex()

  tags = []
  if (tag =~ /kIRG_[A-Z]+Source/)
    toks = dictIndice.split("-")
    tag = toks.shift()
    dictIndice = toks.join("-")
    if (tag =~ /J[0-4A][0-4A]/)
      tags << ("J" + tag[1..1])
      tags << ("J" + tag[2..2])
    else
      tags << tag
    end
  else
    tags << tag
  end
  tags.each do |tag|
    mapDict[tag] = Set.new() unless (mapDict.include?(tag))
    mapDict[tag] << ucsInt
  end
end
STDERR.puts()

ojs = Hash.new()
ojs["format"] = "dict/uhex,delta"
[
  "J0", "J1", "J3", "J4", "JA", "JK", "JH", "JMJ",
  "kKangXi",
  "kMorohashi",
  "kHanYu"
].each do |tag|
  ranges = []
  mapDict[tag].sort().each do |ucsInt|
    if (ranges.last != nil && (ranges.last.last + 1 == ucsInt))
      ranges.last.pop() if (ranges.last.length == 2)
      ranges.last.push(ucsInt)
    else
      ranges.push([ucsInt])
    end
  end
  ojs[tag] = ranges.map{|rng|
    if (rng.length == 1)
      sprintf("U+%04X", rng.first)
    else
      # sprintf("U+%04X-U+%04X", rng.first, rng.last)
      sprintf("U+%04X,%d", rng.first, rng.last - rng.first)
    end
  }
end

if (Opts.pretty)
  puts JSON.pretty_generate(ojs)
else
  puts JSON.generate(ojs)
end
