class DB_Coverage {
  constructor() {
    this.src2uhexs = new Map();
    this.srcs = new Array();
    this.ready = false;
  }

  async load_url(url) {
    let resp = await fetch(url);
    let js = await resp.json();
    for (const dict in js) {
      if (dict == "format")
        continue;
      this.srcs.push(dict);
      this.src2uhexs.set(dict, new Set());
      for (const rng of js[dict]) {
        if (rng.includes(",")) {
          let [uhex_base, delta] = rng.split(",");
          for (let i = 0; i <= delta; i++) {
            let uhex = UHex.int2uhex(UHex.uhex2int(uhex_base) + i);
            this.src2uhexs.get(dict).add(uhex);
          }
        } else {
          this.src2uhexs.get(dict).add(rng);
        }
      }
      // console.log(dict + " has " + this.src2uhexs.get(dict).size.toString() + " codepoints");
    }
    this.ready = true;
  }

  async load_url_vs(url) {
    let resp    = await fetch(url);
    let rawData = await resp.json();

    for (const dict of rawData.meta.dicts) {
      if (!this.src2uhexs.has(dict))
        this.src2uhexs.set(dict, new Set());

      for (const [uhex_base, vsrs] of Object.entries(rawData[dict])) {
        for (const vsr of vsrs.split(";")) {
          if (vsr.includes(",")) {
            let [vsr_base, vsr_delta ] = vsr.split(",");
            vsr_base = parseInt(vsr_base, 16);
            vsr_delta = parseInt(vsr_delta);
            for (let i = 0; i < vsr_delta; i++) {
              this.src2uhexs.get(dict).add(uhex_base + "_" + UHex.int2hex(vsr_base + i));
            }
          } else {
            this.src2uhexs.get(dict).add(uhex_base + "_" + vsr);
          }
        }
      }
    }
    this.ready = true;
  }

  get_srcs_from_uhexs(uhexs, srcs) {
    let o = {};
    for (const uhex of uhexs) {
      if (o[uhex] != undefined)
        continue;

      let srcs_uhex = [];
      for (const s of srcs) {
        if (!this.srcs.includes(s))
          continue;

        if (this.src2uhexs.get(s).has(uhex))
          srcs_uhex.push(s);
      }
      o[uhex] = srcs_uhex;
    }
    return o;
  }

  filter_uhexs_by_src(uhexs, src) {
    const m = this.src2uhexs.get(src)
    return uhexs.filter((u) => m.has(u));
  }
}
