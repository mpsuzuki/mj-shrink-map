#!/usr/bin/env ruby
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "json"
require "set"
require "./ruby-utils/getOpts.rb"

def uhex2utf8(uhex)
  return [uhex[2..-1].hex()].pack("N").encode("utf-8", "ucs-4be")
end

injs = JSON.parse(STDIN.read())
outjs = Array.new()

mapNames_set = Set.new()
injs.each{|hs| hs["maps"].keys().each{|k| mapNames_set << (k)} if (hs.include?("maps"))}
mapNames = mapNames_set.to_a().sort()

injs.each do |hs|
  next if (hs.length < 2)

  ucss = hs["ucss"].split(/\s*,\s*/)
  if (Opts.debug)
    STDERR.printf("ucss=%s\n", ucss.map{|v|
      [v, uhex2utf8(v)].join(":")
    }.join(", "))
  end
  maps = Array.new()
  hs["maps"].each do |mapName, mapHash|
    mapNames << mapName unless (mapNames.include?(mapName))
    mapNameIndex = mapNames.index(mapName)

    mapHash.each do |mapPair|
      idxSrc = ucss.index(mapPair.first)
      idxDsts = mapPair.last.split(/\s*,\s*/).map{|v| ucss.index(v)}
      if (Opts.debug)
        STDERR.printf("%s(%s) -> %s\n", mapPair.first, uhex2utf8(mapPair.first), idxSrc.to_s())
        STDERR.printf("  %s -> %s\n", mapPair.last, idxDsts.to_s())
      end
      idxDsts.each do |idxDst|
        lks = maps.select{|lk| (lk[0] == idxSrc) && (lk[1] == idxDst)}
        if (lks.length > 0)
          lks.first[2] << mapNameIndex
        else
          maps << [idxSrc, idxDst, [mapNameIndex]]
        end
      end
    end
  end
  outjs << [ucss, maps]
end
outjs << {"meta": true, "mapNames": mapNames}

if (Opts.pretty)
  puts JSON.pretty_generate(outjs)
else
  puts JSON.generate(outjs)
end
