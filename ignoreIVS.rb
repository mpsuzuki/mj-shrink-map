#!/usr/bin/ruby3.0
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "set"
require "json"
require "./ruby-utils/getOpts.rb"
require "./ruby-utils/utils.rb"
require "./ruby-utils/rotator.rb"

hsi = JSON.parse(STDIN.read())
hso = Array.new()

hsi.each do |hsm|
  hsu = Hash.new()
  hsu["ucss"] = hsm["mjs"].values().compact().map{|s|
    s.gsub(/[\,\s].*$/, "")
  }.uniq()
  hsu["ucss"] += hsm["ucss"]
  next if (hsu["ucss"].length == 0)
  hso << hsu
  next if (!hsm.include?("maps"))

  hsu["maps"] = Hash.new()
  hsm["maps"].each do |k, hs|
    hsu["maps"][k] = Hash.new()
    hs.each do |s_ivs, d_ucss|
      s_ucs = s_ivs.split(" ").first
      if (!hsu["maps"][k].include?(s_ucs))
        hsu["maps"][k][s_ucs] = d_ucss
      else
        d_ucss.each do |d_ucs|
          next if (hsu["maps"][k][s_ucs].include?(d_ucs))
          hsu["maps"][k][s_ucs] << d_ucs
        end
      end
    end
  end
end

hso.each do |hsu|
  hsu["ucss"] = hsu["ucss"].join(", ")
  next if (!hsu.include?("maps"))
  hsu["maps"].each do |k, hs|
    hs.each do |s_ucs, d_ucss|
      hsu["maps"][k][s_ucs] = d_ucss.join(", ")
    end
  end
end



puts JSON.pretty_generate(hso)
