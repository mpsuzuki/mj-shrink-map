class DB_uhex2ivs {
  constructor() {
    this.uhex2ivs = new Map();
    this.noIVD = false;
    this.ready = false;
    this.textDecoder = new TextDecoder();
    this.useConsole = false;
  }

  static expand_hex_range_hyphen_cont(s) {
    const hexs = new Array();
    const toks = s.split("-")
    const hexFirst = parseInt(toks[0], 16);
    const hexLast = parseInt(toks.pop(), 16);
    for (let i = hexFirst; i <= hexLast; i ++) {
      hexs.push(i.toString(16).toUpperCase());
    }
    return hexs;
  }

  static expand_hex_range_comma_discont(s) {
    const hexs = new Set();
    const toks = s.split(",");
    toks.forEach(function(t) {
      DB_uhex2ivs.expand_hex_range_hyphen_cont(t).forEach(function(h) {
        hexs.add(h);
      });
    });
    return hexs;
  }

  static get_suffix_lo(s) {
    return s.split(".").pop().toLowerCase();
  }

  async _appendURL_json(url) {
    this.noIVD = false;
    let resp = await fetch(url);
    const js = await resp.json();
    let ojs = js;
    for (const [ivd_name, ivd_db] of Object.entries(ojs)) {
      if (!this.uhex2ivs.has(ivd_name))
        this.uhex2ivs.set(ivd_name, new Map());

      for (const [uhex_base, vs_range] of Object.entries(ivd_db)) {
        this.uhex2ivs.get(ivd_name).set(uhex_base, DB_uhex2ivs.expand_hex_range_comma_discont(vs_range));
      }
    }
    this.ready = true;
  }

  async _appendURL_txt(url) {
    const db = this;
    await fetch(url).then(async (resp) => {
      const uint8_CR = "\r".codePointAt(0);
      const uint8_LF = "\n".codePointAt(0);
      const len_total = resp.headers.get("content-length");
      let len_done = 0;

      let reader = resp.body.getReader();
      let chunks = [];
      let procResult = async function(resl) {
        if (resl.done) {
          db.ready = true;
          return;
        }

        const len_chunk = resl.value.length;

        if (resl.value) {
          len_done += resl.value.length;
        }

        const ends_with_LF = (uint8_LF == resl.value[len_chunk - 1]);

        if (db.useConsole) {
          console.log([len_done, len_total].map((l) => l.toString()).join("/"));
          console.log('    current chunk starts "' +
                      db.textDecoder
                        .decode(resl.value.slice(0, 10))
                        .replace("\n", "\\n") +
                      '..."');
          if (ends_with_LF)
            console.log('    current chunk ends with LF');
          else
            console.log('    current chunk ends "...' +
                        db.textDecoder
                          .decode(resl.value.slice(len_chunk - 10, len_chunk))
                          .replace("\n", "\\n") +
                        '"');
        }

        chunks.push(db.textDecoder.decode(resl.value));
        let lines = chunks.join("").split("\n");
        if (db.useConsole) {
          console.log("  " + lines[0]);
        }
        chunks.splice(0);
        chunks.push( lines.pop() );
        if (ends_with_LF)
          chunks.push( "\n" );

        lines.forEach((l) => {
          if (l.startsWith("#"))
            return;

          let toks = l.split(/\s*;\s*/);
          const base_ivs   = toks.shift();
          const ivd_name   = toks.shift();
          const glyph_name = toks.shift();

          toks = base_ivs.split(/\s+/);
          const uhex_base = "U+" + toks[0];
          const hex_vs = toks[1];

          if (!db.uhex2ivs.has(ivd_name))
            db.uhex2ivs.set(ivd_name, new Map());

          if (!db.uhex2ivs.get(ivd_name).has(uhex_base))
            db.uhex2ivs.get(ivd_name).set(uhex_base, new Set());

          db.uhex2ivs.get(ivd_name).get(uhex_base).add(hex_vs);
        });

        await reader.read().then(procResult);
      };

      await reader.read().then(procResult);
    });
  }

  async appendURL(url, ivd) {
    switch(DB_uhex2ivs.get_suffix_lo(url)) {
    case "json":
      await this._appendURL_json(url);
      break;
    case "txt":
      await this._appendURL_txt(url);
      break;
    }
  }

  get_ivs_from_uhex(uhex, ivd) {
    if (ivd == null || typeof(ivd) == "undefined")
      ivd = "";

    if (this.noIVD || ivd.length == 0) {
      let vs = new Set();
      for (const n of this.uhex2ivs.keys()) {
        const uhex_ivs = this.uhex2ivs.get(n).get(uhex);
        if (!uhex_ivs)
          continue;

        for (const v of uhex_ivs)
          vs.add(v);
      }

      return Array.from(vs).sort();
    } else
    if (!this.uhex2ivs.has(ivd))
      return [];
    else
    if (!this.uhex2ivs.get(ivd).has(uhex))
      return [];
    else
      return Array.from(this.uhex2ivs.get(ivd).get(uhex)).sort();
  }

  get_ivs_dict_from_uhexs(uhexs, ivd) {
    let ivs_dict = new Object();
    let _this = this;
    uhexs.forEach(function(uhex){
      ivs_dict[uhex] = Array.from(_this.get_ivs_from_uhex(uhex, ivd));
    });
    return ivs_dict;
  }
}
