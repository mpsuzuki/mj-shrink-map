importScripts("cls-uhex.js");
importScripts("cls-coverage.js");

db = new DB_Coverage();

onmessage = function(evt) { 
  let msgJS = JSON.parse(evt.data);
  let retJS = new Object();
  retJS.op = msgJS.op;

  switch (msgJS.op) {
  case "ready?":
  case "status":
    retJS.status = "ok";
    retJS.ready = db.ready;
    postMessage(JSON.stringify(retJS));
    break;

  case "load":
    db.load_url(msgJS.url)
      .then(() => {
         retJS.status = "ok";
         retJS.ready = db.ready;
         retJS.src2counts = {};
         for (const s of db.srcs) {
           retJS.src2counts[s] = db.src2uhexs.get(s).size;
         }
         postMessage(JSON.stringify(retJS));
      });
    break;

  case "load_vs":
    db.load_url_vs(msgJS.url)
      .then(() => {
         retJS.status = "ok";
         retJS.ready = db.ready;
         retJS.src2counts = {};
         for (const s of db.srcs) {
           retJS.src2counts[s] = db.src2uhexs.get(s).size;
         }
         postMessage(JSON.stringify(retJS));
      });
    break;

/*
  case "clear":
  case "reset":
    db.clear();
    retJS.status = "ok";
    postMessage(JSON.stringify(retJS));
    break;
*/

  case "get_srcs_from_uhexs":
    if (!db.ready) {
      retJS.status = "failed";
    } else {
      retJS.uhex2srcs = db.get_srcs_from_uhexs(msgJS.uhexs, msgJS.srcs);
      retJS.status = "ok";
    }
    postMessage(JSON.stringify(retJS));
    break;

  case "filter_uhexs_by_src":
    if (!db.ready || !db.src2uhexs.has(msgJS.src)) {
      retJS.status = "failed";
    } else {
      retJS.uhexs = db.filter_uhexs_by_src(msgJS.uhexs, msgJS.src);
      retJS.status = "ok";
    }
    postMessage(JSON.stringify(retJS));
    break;

  default:
    retJS.status = "unknown command";
    postMessage(JSON.stringify(retJS));
    break;
  }
};
