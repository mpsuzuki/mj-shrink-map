class Menu_LinkRef {
  constructor () {
    this.index2name = new Map();
    this.name2index = new Map();
    this.name2elm   = new Map();
  }

  get_map_name_indexes() {
    let map_name_indexes = new Set();
    for (const [nm, elm] of this.name2elm) {
      if (!this.name2index.has(nm))
        continue;

      map_name_indexes.add( this.name2index.get(nm) );
    }
    return map_name_indexes;
  }

  get_map_name_indexes_checked() {
    return new Set( Array.from( this.get_map_name_indexes() )
                         .filter((idx) => {
                            // if (!this.index2name.has(idx))
                            //   return false;
                            // if (!this.name2elm.has( this.index2name.get(idx) ))
                            //   return false;

                            return this.name2elm.get( this.index2name.get(idx) ).checked;
                         }).sort() );
  }

  create_span(obj) {
    const elmSPAN = document.createElement("span");
    const elmCB = document.createElement("input");
    elmCB.setAttribute("type", "checkbox");
    elmSPAN.appendChild(elmCB);
    elmCB.check_for = obj.fullname;
    elmCB.setAttribute("check-for", obj.fullname);
    if (obj.checked != undefined) {
      elmCB.checked = obj.checked;
    }

    elmCB.addEventListener("click", (evt) => {
      evt.stopPropagation();
      // console.log("click checkbox for " + obj.fullname);
      console.log(this.get_map_name_indexes_checked());
    });

    if (obj.label)
      elmSPAN.appendChild(document.createTextNode(obj.label));
    else
      elmSPAN.appendChild(document.createTextNode(obj.fullname));

    elmSPAN.addEventListener("click", (evt) => {
      evt.stopPropagation();
      // console.log("click span for " + obj.fullname);
      elmCB.click();
    });


    this.name2elm.set(obj.fullname, elmCB);
    if (obj.label)
      this.name2elm.set(obj.label, elmCB);

    return elmSPAN;
  }

  add_section_to_div(obj, elmDIV) {
    const elmDIV2 = document.createElement("div");
    elmDIV2.classList.add("family");
    elmDIV.appendChild(elmDIV2);
    const elmSPAN = document.createElement("span");
    elmDIV2.appendChild(elmSPAN);
    elmSPAN.classList.add("prefix");
    elmSPAN.appendChild(document.createTextNode(obj.prefix));
    elmDIV2.appendChild(document.createElement("br"));
    for (const obj2 of obj.member) {
      if (obj2 == null) {
        elmDIV2.appendChild(document.createElement("br"));
      } else
      if (obj2.prefix) {
        this.add_section_to_div(obj2, elmDIV2);
      } else
      if (obj2.fullname) {
        elmDIV2.appendChild(this.create_span(obj2));
      }
    }
    elmSPAN.addEventListener("click", (evt) => {
      // console.log("check span for " + evt.currentTarget.textContent);
      evt.stopPropagation();
      for (const elmCB of evt.currentTarget.parentElement.querySelectorAll("input[type='checkbox']")) {
        // console.log(elmCB);
        elmCB.click();
      }
    });
  }

  construct_elm_from_json(url, cls) {
    return new Promise((fResolve, fReject) => {
      fetch(url)
      .then((resp) => resp.json())
      .then((js) => {
        let elmDIV_root = document.createElement("div");
        elmDIV_root.classList.add(cls);
        // elmDIV_root.setAttribute("id", "selector_menu");

        for (const obj of js) {
          if (obj == null) {
            elmDIV_root.appendChild(document.createElement("br"));
          } else
          if (obj.prefix) {
            this.add_section_to_div(obj, elmDIV_root);
          } else
          if (obj.fullname) {
            elmDIV_root.appendChild(this.create_span(obj));
            elmDIV_root.appendChild(document.createElement("br"));
          }
        }
        fResolve(elmDIV_root);
      })
    })
  }

  get_width_of_elm(elm, elm_parent) {
    elm_parent.appendChild(elm);
    let div_width = Math.max( ...Array.from(elm.querySelectorAll("span, div.family"))
                                      .map((e) => e.getBoundingClientRect())
                                      .map((r) => r.x + r.width)
                            );
    let left_margin = Math.min( ...Array.from(elm.querySelectorAll("input[type='checkbox']"))
                                        .map((e) => e.getBoundingClientRect())
                                        .map((r) => r.x)
                              );
    let height_visible = document.body.clientHeight - elm.getBoundingClientRect().top;
    let height_content = elm.clientHeight;
    if (height_visible < height_content)
      return (div_width + (left_margin * 2)); // consider the width of scroll bar
    else
      return (div_width + left_margin);
  }
}
