#!/usr/bin/env ruby
require "json"
require "set"
Opts = {
  "ivs-offset" => 0,
  "unused" => false,
  "base" => true,
  "none" => true
}
require "./ruby-utils/getOpts.rb"

class IVSRange
  attr_accessor(:first, :delta)
  def initialize(h)
    @first = h.hex()
    @delta = 1
  end

  def append_or_new(h)
    if ((@first + @delta) == h.hex())
      @delta += 1
      return self
    else
      return IVSRange.new(h)
    end
  end

  def to_s()
    if (1 < @delta)
      return sprintf("%X,%d", @first, @delta)
    else
      return sprintf("%X", @first)
    end
  end
end

def arr2ivsRanges(arr)
  rs = [ IVSRange.new(arr.shift()) ]
  while (arr.length > 0)
    r = rs.last.append_or_new(arr.shift())
    rs.push(r) if (rs.last != r)
  end
  return rs
end

js = JSON.parse(STDIN.read())

dicts = Set.new()
js.each do |uhex, hs0|
  hs0.each do |uhex_vs, hs1|
    hs1.each do |mj, hs2|
      hs2.keys().each do |dict|
        dicts << dict
      end
    end
  end
end

if (Opts.dicts)
  dicts = Opts.dicts.split(",")
end

mini_json = Hash.new()
mini_json["meta"] = { "content" => "ivs-only", "range" => [], "ivsOffset" => Opts["ivs-offset"] }
mini_json["meta"]["dicts"] = dicts.map{|dict| "VS_" + dict}
dicts.each do |dict|
  vs_dict = "VS_" + dict
  mini_json[vs_dict] = Hash.new()
end

hex_base_min = 0xFFFFF
hex_base_max = 0
hexs_used = Set.new()

js.each do |uhex_base, hs0|
  hex_base = uhex_base.gsub("U+", "").hex()
  hex_base_min = [hex_base, hex_base_min].min()
  hex_base_max = [hex_base, hex_base_max].max()

  hs0.each do |uhex_vs, hs1|
    next unless (uhex_vs.include?("_"))
    hs1.each do |mj, hs2|
      hs2.keys().each do |dict|
        next if (!dicts.include?(dict))
        vs_dict = "VS_" + dict
        mini_json[vs_dict] = Hash.new() unless (mini_json.include?(vs_dict))
        mini_json[vs_dict][uhex_base] = Array.new() unless (mini_json[vs_dict].include?(uhex_base))
        mini_json[vs_dict][uhex_base] << sprintf("%X", uhex_vs.split("_").pop().hex() - mini_json["meta"]["ivsOffset"])
      end
    end
  end
end
mini_json["meta"]["range"] << sprintf("U+%04X", hex_base_min)
mini_json["meta"]["range"] << sprintf("U+%04X", hex_base_max)

# p mini_json

mini_json["meta"]["dicts"].each do |vs_dict|
  mini_json[vs_dict].keys().each do |uhex_base|
    mini_json[vs_dict][uhex_base] = arr2ivsRanges( mini_json[vs_dict][uhex_base].sort() ).join(";")
    v = mini_json[vs_dict][uhex_base]
    if (!v.include?(",") && !v.include?(";") && v.hex() < 100)
      mini_json[vs_dict][uhex_base] = v.hex()
    end
  end
end

if (Opts.pretty)
  puts JSON.pretty_generate(mini_json)
else
  puts JSON.generate(mini_json)
end
