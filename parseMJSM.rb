#!/usr/bin/ruby3.0
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "set"
require "json"
require "./ruby-utils/getOpts.rb"
require "./ruby-utils/utils.rb"
require "./ruby-utils/rotator.rb"

rot = Rotator.new(1000)

def uhex2utf8(uhex)
  return [uhex[2..-1].hex()].pack("N").encode("utf-8", "ucs-4be")
end

class IVS
  attr_accessor(:name, :ivs, :ucs_base, :ucs_vs, :hanyo_denshi)
  def initialize(name, ivs)
    @name = name
    @ivs = ivs
    @ucs_base, @ucs_vs, = ivs.split(/\s+/, 2).map{|t| "U+" + t}
    return self
  end

  def ucs_ivs()
    return [@ucs_base, @ucs_vs].compact().join(" ")
  end

  def to_hs()
    r = Hash.new()
    r["name"] = r.name
    r["base"] = r.ucs_base
    r["vs"]   = r.ucs_vs
    return r
  end

  def get_hd_prefix_type_num(hd)
    return ["JA", "JB", "JC", "JD", "JE",
            "JF", "JM", "FT", "IA", "IB",
            "HG", "JT", "KS", "TK", "FT",
            "IP", "AR"].index(hd[0..1])
  end

  def get_hds()
    return nil if (!@hanyo_denshi || @hanyo_denshi.length < 1)

    return @hanyo_denshi.sort{|hd0, hd1|
      if (hd0[0..1] != hd1[0..1])
        self.get_hd_prefix_type_num(hd0) <=> self.get_hd_prefix_type_num(hd1)
      else
        hd0 <=> hd1
      end
    }.join(" ")
  end

  def ucs_base_utf8()
    return uhex2utf8(@ucs_base)
  end
end

class GlyphSet
  attr_accessor(:sid)
  attr_accessor(:mjs, :ucss, :maps)
  def initialize()
    @mjs  = Set.new()
    @ucss = Set.new()
    @maps = Hash.new()
  end

  def mergeWith(gs)
    gs.mjs.each{|mj| self.mjs.add(mj)}
    gs.ucss.each{|ucs| self.ucss.add(ucs)}
    gs.maps.each do |k, v|
      @maps[k] = Hash.new() if (!@maps.include?(k))
      v.each do |mj, ucss|
        @maps[k][mj] = Array.new() if (!@maps.include?(mj))
        ucss.each do |ucs|
          @maps[k][mj] << ucs if (!@maps[k][mj].include?(ucs))
        end
      end
    end
    return self
  end

  def mergeTo(gs)
    self.mjs.each{|mj| gs.mjs.add(mj)}
    self.ucss.each{|ucs| gs.ucss.add(ucs)}
    return gs
  end

  def to_hs(mj2ivs)
    r = Hash.new()
    r["sid"] = @sid
    r["mjs"] = Hash.new()
    @mjs.to_a().sort().each do |mj|
      if (mj2ivs.include?(mj))
        r["mjs"][mj] = [ [ mj2ivs[mj].ucs_base, mj2ivs[mj].ucs_vs ].compact().join(" "),
                         mj2ivs[mj].get_hds()
                       ].compact().join(", ")
      else
        r["mjs"][mj] = nil
      end
    end

    # r["ucss"] = @ucss.to_a().sort()
    r["ucss"] = (@ucss - @mjs.filter_map{|mj| mj2ivs.include?(mj) && mj2ivs[mj].ucs_base}.uniq()).to_a()
    r["maps"] = Hash.new()
    @maps.keys().sort().each do |k|
      r["maps"][k] = Hash.new()
      @maps[k].keys().sort().each do |mj|
        ivs = mj2ivs[mj]
        if (k.include?("辞書"))
          if (ivs)
            r["maps"][k][ivs.ucs_ivs] = @maps[k][mj].sort().select{|ucs| ucs != ivs.ucs_base}
          else
            r["maps"][k][mj] = @maps[k][mj].sort()
          end
        else
          if (ivs)
            r["maps"][k][ivs.ucs_ivs] = @maps[k][mj].select{|ucs| ucs != ivs.ucs_base}
          else
            r["maps"][k][mj] = @maps[k][mj]
          end
        end
      end
    end
    r["maps"].keys().each do |k|
      if (r["maps"][k].length == 0)
        r["maps"].delete(k)
      end
    end
    if (r["maps"].length == 0)
      r.delete("maps")
    end
    return r
  end

  def ucss_utf8()
    return @ucss.map{|ucs| sprintf("%s(%s)", ucs, uhex2utf8(ucs))}
  end
end

fh = File::open(Opts.args.select{|fp| fp.suffix_lowercase() == "json"}.first, "r")
mjsm = JSON.parse(fh.read())
fh.close()

mj2ivs = Hash.new()
ivs2hd = Hash.new()
Opts.args.select{|fp|
  fpd = fp.downcase()
  fpd.include?("ivd") && fpd.split(".").last == "txt"
}.each do |fp|
  fh = File::open(fp, "r")
  while (fh.gets())
    STDERR.printf(rot.get()) if (!Opts.debug)
    if ($_.gsub(/#.*/, "").count(";") < 2)
      next
    else
      ivs, coll, gname, = $_.chomp().split(/\s*;\s*/, 3)
      if (coll == "Moji_Joho")
        ivs = IVS.new(gname, ivs)
        mj2ivs[ivs.name] = ivs
      elsif (coll == "Hanyo-Denshi")
        ivs2hd[ivs] = Set.new() if (!ivs2hd.include?(ivs))
        ivs2hd[ivs] << gname
      end
    end
  end
  fh.close()
end

Opts.args.select{|fp| fp.include?("hd2ucs.txt")}.each do |fp|
  fh = File::open(fp, "r")
  hd2ucs = Hash.new()
  hd2hd = Hash.new()
  while (fh.gets())
    STDERR.printf(rot.get()) if (!Opts.debug)
    toks = $_.chomp.split(/\s+/).map{|v| v.gsub(/\[[U0-9]+\]/, "").gsub("*", "").gsub("=", "").gsub("~", "")}
    if (toks[1] =~ /U\+/)
      if (toks[1] =~ /U\+0[0-9A-F]{4}/)
        ucs = "U+" + toks[1][3..-1]
      else
        ucs = toks[1]
      end
      hd2ucs[toks[0]] = ucs
    else
      hd2hd[toks[0]] = toks[1]
    end
  end
  fh.close()
  while (hd2hd.length > 0)
    STDERR.printf(rot.get())
    hd2hd_ = Hash.new()
    hd2hd.each do |s_hd, d_hd|
      if (hd2ucs.include?(d_hd))
        hd2ucs[s_hd] = hd2ucs[d_hd]
      else
        hd2hd_[s_hd] = d_hd
      end
    end
    break if (hd2hd_.length == hd2hd.length)
    hd2hd = hd2hd_
  end
  hd2ucs.each do |hd1, ucs|
    ivs2hd[ucs[2..-1]] = Set.new() if (!ivs2hd.include?(ucs[2..-1]))
    ivs2hd[ucs[2..-1]] << hd1
  end
end
# p ivs2hd

Opts.args.select{|fp|
  "csv" == fp.split(".").last.downcase()
}.each do |fp|
  fh = File::open(fp, "r")
  while (fh.gets())
    STDERR.printf(rot.get()) if (!Opts.debug)
    toks = $_.chomp().split(",")
    toks.shift() # discard an arrow
    toks.shift() # discard a raw character
    mj = toks.shift()
    next if (mj !~ /^MJ[0-9]{6}$/)
    next if (mj2ivs.include?(mj))

    ucs0 = toks.shift() # corresponding UCS
    ucs1 = toks.shift() # implemented UCS
    ivs  = toks.shift()

    if (ivs.length > 0)
      ivs = ivs.gsub("_", " ")
      mj2ivs[mj] = IVS.new(mj, ivs)
    elsif (ucs0.length > 0)
      mj2ivs[mj] = IVS.new(mj, ucs0.gsub("U+", ""))
    elsif (ucs1.length > 0)
      mj2ivs[mj] = IVS.new(mj, ucs1.gsub("U+", ""))
    end
  end
  fh.close()
end

mj2ivs.each do |mj, ivs|
  if (ivs2hd.include?(ivs.ivs))
    ivs.hanyo_denshi = ivs2hd[ivs.ivs]
  end
  # p [mj, ivs]
end


ucs2sid = Hash.new()
glyphSets = Array.new()

mjsm["content"].each do |entry|
  mj = entry["MJ文字図形名"]
  if (Opts.debug)
    STDERR.printf("proc entry for %s\n", mj)
  else
    STDERR.printf(rot.get())
  end

  ucss = Set.new()
  if (mj2ivs.include?(mj))
    ucs = mj2ivs[mj].ucs_base
    ucss << ucs
    STDERR.printf("  add ucs=%s(%s) to ucss, as base of mj=%s(%s)\n",
                  ucs, uhex2utf8(ucs), mj, mj2ivs[mj].ucs_base_utf8()) if (Opts.debug)
  end
  entry.each do |k, v|
    next if (k == "MJ文字図形名")
    if (v.isArray())
      v.select{|hs| hs.include?("UCS")}.each do |hs|
        ucs = hs["UCS"]
        ucss << ucs
        STDERR.printf("    add ucs=%s(%s) to ucss, as related of mj=%s(%s)\n",
                      ucs, uhex2utf8(ucs), mj, mj2ivs[mj].ucs_base_utf8()) if (Opts.debug)
      end
    end    
  end

  # idxs = (0...(glyphSets.length)).to_a().select{|i| glyphSets[i].ucss.intersect?(ucss) }.sort()
  idxs = ucss.map{|ucs| ucs2sid[ucs]}.compact().uniq().sort()
  if (idxs.length == 0)
    gs0 = GlyphSet.new()
    gs0.sid = glyphSets.length
    sid = gs0.sid
    STDERR.printf("  create set#%d_\n", sid) if (Opts.debug)
    glyphSets << gs0
    ucss.each do |ucs|
      STDERR.printf("    add %s(%s) to set#%d_.ucss\n", ucs, uhex2utf8(ucs), sid) if (Opts.debug)
      gs0.ucss << ucs
    end
    gs0.mjs << mj
  else
    sid = idxs.shift()
    gs0 = glyphSets[sid]
    STDERR.printf("  reuse set#%d_ including %s\n", sid, gs0.ucss_utf8().join(", ")) if (Opts.debug)
    ucss.each do |ucs|
      STDERR.printf("    add %s(%s) to set#%d_.ucss\n", ucs, uhex2utf8(ucs), sid) if (Opts.debug)
      gs0.ucss << ucs
    end
    gs0.mjs << mj
    idxs.each do |idx|
      STDERR.printf("    reuse %d-th set#%d_ into %d-th set#%d_\n",
                    idx, glyphSets[idx].sid, sid, gs0.sid) if (Opts.debug)
      gs0.mergeWith(glyphSets[idx])
      glyphSets[idx] = nil
    end
  end
  gs0.ucss.each do |ucs|
    STDERR.printf("  ucs2sid[%s(%s)] = %d <- %d\n",
                  ucs, uhex2utf8(ucs), sid, ucs2sid[ucs] ? ucs2sid[ucs] : -1 ) if (Opts.debug)
    ucs2sid[ucs] = sid
  end

  entry.each do |k, v|
    if (v.isArray())
      v.select{|hs| hs.include?("UCS")}.each do |hs|
        STDERR.printf("  proc submap entry %s\n", hs.to_s()) if (Opts.debug)
        k_full_toks = [k]
        if (hs.include?("表"))
          k_full_toks << ("表" + hs["表"])
        end
        if (hs.include?("順位"))
          k_full_toks << hs["順位"]
        end
        if (hs.include?("種別"))
          k_full_toks << hs["種別"]
        end
        if (hs.include?("ホップ数"))
          k_full_toks << ("ホップ" + hs["ホップ数"].to_s())
        end
        k_full = k_full_toks.join("\n")
        if (!gs0.maps.include?(k_full))
          gs0.maps[k_full] = Hash.new
          gs0.maps[k_full][mj] = Array.new()
        end
        if (gs0.maps[k_full].include?(mj))
          if (!gs0.maps[k_full][mj].include?(hs["UCS"]))
            gs0.maps[k_full][mj] << hs["UCS"]
          end
        else
          gs0.maps[k_full][mj] = [ hs["UCS"] ]
        end
        STDERR.printf("    gs[%d].maps[%s][%s=U+%s(%s)] << %s(%s)\n",
                      gs0.sid, k_full,
                      mj, mj2ivs[mj].ivs, mj2ivs[mj].ucs_base_utf8(),
                      hs["UCS"], uhex2utf8(hs["UCS"])
        ) if (Opts.debug)
      end
    end
  end
end

glyphSets.each do |gs|
  next if (!gs)

  gs.maps.each do |k, v|
    gs.maps[k].each do |mj, ucss|
      if (!mj2ivs.include?(mj))
        STDERR.printf("map-%s has unencoded %s\n", k, mj) if (Opts.debug)
        next
      end
      ivs = mj2ivs[mj]
      gs.maps[k][mj] -= [ ivs.ucs_base ]
      STDERR.printf("set#%d_ exclude %s(%s) itself %s from map-%s\n",
                    gs.sid, ivs.ucs_base, ivs.ucs_base_utf8, mj, k) if (Opts.debug)
    end
    gs.maps[k].keys().each do |mj|
      if (gs.maps[k][mj].length == 0)
        STDERR.printf("set#%d_ delete empty submap for %s(%s) from map-%s\n",
                      gs.sid, mj, mj2ivs[mj].ucs_base_utf8(), k) if (Opts.debug)
        gs.maps[k].delete(mj)
      end
    end
    gs.maps.keys().each do |k|
      if (gs.maps[k].length == 0)
        STDERR.printf("set#%d_ delete empty map-%s\n", gs.sid, k) if (Opts.debug)
        gs.maps.delete(k)
      end
    end
  end
end

puts JSON.pretty_generate( glyphSets.compact().map{|gs| gs.to_hs(mj2ivs)} )
# puts JSON.generate( glyphSets.compact().map{|gs| gs.to_hs(mj2ivs)} )
