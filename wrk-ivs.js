importScripts("cls-uhex2ivs.js");

db = new DB_uhex2ivs();

onmessage = function(evt) { 
  let msgJS = JSON.parse(evt.data);
  let retJS = new Object();
  retJS.op = msgJS.op;

  switch (msgJS.op) {
  case "ready?":
  case "status":
    retJS.status = "ok";
    retJS.ready = db.ready;
    postMessage(JSON.stringify(retJS));
    break;

  case "load":
    db.appendURL(msgJS.url)
      .then(() => {
         retJS.status = "ok";
         retJS.ready = db.ready;
         postMessage(JSON.stringify(retJS));
      });
    break;

  case "clear":
  case "reset":
    db.clear();
    retJS.status = "ok";
    postMessage(JSON.stringify(retJS));
    break;

  case "get_ivs_from_uhex":
    if (!db.ready) {
      retJS.status = "failed";
    } else {
      retJS.ivs = db.get_ivs_from_uhex(msgJS.uhex, msgJS.ivd);
      retJS.status = "ok";
    }
    postMessage(JSON.stringify(retJS));
    break;

  case "get_ivs_dict_from_uhexs":
    if (!db.ready) {
      retJS.status = "failed";
    } else {
      retJS.ivs_dict = db.get_ivs_dict_from_uhexs(msgJS.uhexs, msgJS.ivd);
      retJS.status = "ok";
    }
    postMessage(JSON.stringify(retJS));
    break;

  default:
    retJS.status = "unknown command";
    postMessage(JSON.stringify(retJS));
    break;
  }
};
