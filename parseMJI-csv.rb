#!/usr/bin/env ruby
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "csv"
require "json"
require "./ruby-utils/rotator.rb"

mj2dictSeq = Hash.new()
uhexBase2dictSeq = Hash.new()
dict2idx = Hash.new()

heads = STDIN.gets().split(",")
heads.each_with_index do |hd, idx|
  case hd
  when "大漢和" then
    dict2idx["Morohashi"] = idx
  when "日本語漢字辞典" then
    dict2idx["Shincho"] = idx
  when "新大字典" then
    dict2idx["Kodansha"] = idx
  when "大字源" then
    dict2idx["Kadokawa"] = idx
  when "大漢語林" then
    dict2idx["Daikangorin"] = idx
  end
end

STDERR.printf(".")
rot = Rotator.new(1000)

while (STDIN.gets())
  STDERR.printf(rot.get())
  toks = $_.split(",")
  mj = toks[2]
  uhex, uhex_impl, uhex_ivs, uhex_svs, uhex_cmpt = [toks[3, 4], toks[12]].flatten().map{|t| t.length > 0 ? t : nil}

  uhex_ivs = "U+" + uhex_ivs if (uhex_ivs)
  uhex_svs = "U+" + uhex_ivs if (uhex_svs)

  uhex = (uhex_ivs ? uhex_ivs :
           (uhex_svs ? uhex_svs :
             (uhex_impl ? uhex_impl : uhex)
           )
         )

  if (uhex == nil)
    STDERR.printf("\n")
    STDERR.printf("no ucs: %s", $_)
    STDERR.printf("\n")
    next
  end
  uhex_base = uhex.split("_").first

  mj2dictSeq[mj] = Hash.new() unless (mj2dictSeq.include?(mj))
  mj2dictSeq[mj][uhex_base] = Hash.new() unless (mj2dictSeq[mj].include?(uhex_base))
  mj2dictSeq[mj][uhex_base][uhex] = Hash.new() unless (mj2dictSeq[mj][uhex_base].include?("uhex"))

  uhexBase2dictSeq[uhex_base] = Hash.new() unless (uhexBase2dictSeq.include?(uhex_base))
  uhexBase2dictSeq[uhex_base][uhex] = Hash.new() unless (uhexBase2dictSeq[uhex_base].include?(uhex))
  uhexBase2dictSeq[uhex_base][uhex][mj] = Hash.new() unless (uhexBase2dictSeq[uhex_base][uhex].include?(mj))
  uhexBase2dictSeq[uhex_base][uhex][mj]["Cmpt"] = uhex_cmpt if (uhex_cmpt && uhex != uhex_cmpt)
  uhexBase2dictSeq[uhex_base][uhex][mj]["SVS"] = uhex_svs if (uhex_svs && uhex != uhex_svs)

  dict_cnt = 0
  ["Morohashi", "Shincho", "Kodansha", "Kadokawa", "Daikangorin"].each do |dict|
    col_idx = dict2idx[dict]
    seq = toks[col_idx]
    next if (seq.length == 0)
    if (seq =~ /^[0-9]+$/)
      seq = seq.to_i()
    elsif (seq =~ /^補[0-9]+$/)
      seq = sprintf("x%04d", seq[1..-1].to_i())
    end
    mj2dictSeq[mj][uhex_base][uhex][dict] = seq unless (mj2dictSeq[mj][uhex_base].include?("uhex"))
    # p [mj, uhex, dict, seq]

    uhexBase2dictSeq[uhex_base][uhex][mj][dict] = seq
    dict_cnt += 1
  end
  if (dict_cnt == 0)
    uhexBase2dictSeq[uhex_base][uhex][mj]["NoDict"] = true
  end
end
STDERR.printf("\n")

# puts JSON.pretty_generate(mj2dictSeq)
puts JSON.pretty_generate(uhexBase2dictSeq)
