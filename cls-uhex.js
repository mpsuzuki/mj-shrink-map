class UHex {
  static isUHex(s) {
    const rUHex = /^U\+[0-9A-Fa-f]{4,5}$/;
    return (s && s.match(rUHex));
  }

  static startsWithUHex(s) {
    const rUHex = /^U+[0-9A-Fa-f]{4,5}/;
    return (s && s.match(rUHex));
  }

  static hasSVS(s) {
    return /^U\+[0-9A-Fa-f]+_[Ff][Ee][0-9A-Fa-f]{2}$/.test(s);
  }

  static hasIVS(s) {
    return /^U\+[0-9A-Fa-f]+_[Ee][0-9A-Fa-f]{4}$/.test(s);
  }

  static getCJKBlockName(s) {
    if (!s)
      return null;

    const iucs = s.codePointAt(0);

    if (0x3400 <= iucs && iucs <= 0x4DB5)
      return "ExtA";
    if (0x4DB6 <= iucs && iucs <= 0x4DBF)
      return "ExtA_";
    if (0x4E00 <= iucs && iucs <= 0x9FA5)
      return "URO";
    if (0x9FA6 <= iucs && iucs <= 0x9FFF)
      return "URO_";
    if (0xF900 <= iucs && iucs <= 0xFAFF)
      return "CmptBMP";
    if (0x20000 <= iucs && iucs <= 0x2A6D6)
      return "ExtB";
    if (0x2A6D6 <= iucs && iucs <= 0x2A6FF)
      return "ExtB_";
    if (0x2A700 <= iucs && iucs <= 0x2B734)
      return "ExtC";
    if (0x2A735 <= iucs && iucs <= 0x2B73F)
      return "ExtC_";
    if (0x2A740 <= iucs && iucs <= 0x2B81F)
      return "ExtD";
    if (0x2B820 <= iucs && iucs <= 0x2CEA1)
      return "ExtE";
    if (0x2CEB0 <= iucs && iucs <= 0x2EBE0)
      return "ExtF";
    if (0x2EBF0 <= iucs && iucs <= 0x2EED5)
      return "ExtI";
    if (0x2F800 <= iucs && iucs <= 0x2FA1F)
      return "CmptSIP";
    if (0x30000 <= iucs && iucs <= 0x3134A)
      return "ExtG";
    if (0x31350 <= iucs && iucs <= 0x323AF)
      return "ExtH";

    return null;
  }

  static uhex2int(uhex) {
    if (UHex.isUHex(uhex)) {
      return parseInt(uhex.substr(2), 16);
    }
    return null;
  }

  static hex2str(hex) {
    return String.fromCodePoint( parseInt(hex, 16) );
  }

  static uhex2str(uhex) {
    const iucs = UHex.uhex2int(uhex);
    if (iucs) {
      return String.fromCodePoint( iucs );
    }
    return null;
  }

  static c2uhex(c) {
    return ("U+" + c.codePointAt(0).toString(16).toUpperCase());
  }

  static int2hex(i) {
    if (i < 0x10)
      return "000" + i.toString(16).toUpperCase();
    else if (i < 0x100)
      return "00" + i.toString(16).toUpperCase();
    else if (i < 0x1000)
      return "0" + i.toString(16).toUpperCase();
    else
      return i.toString(16).toUpperCase();
  }

  static int2uhex(i) {
    return "U+" + this.int2hex(i);
  }

  static _str2uhexs(s) {
    return Array.from(s)
                .filter((c) => (c != "," && c != " "))
                .map((c) => UHex.c2uhex(c));
  }

  static str2uhexs(s) {
    const rUHex  = /U\+[0-9A-Fa-f]{4,}/;
    const rUHexG = /U\+[0-9A-Fa-f]{4,}/g;
    let toks = new Array();
    let toks_raw  = s.split(rUHex, s.length);
    let toks_uhex = s.match(rUHexG);
    if (s.startsWith("U+")) {
      while (1) {
        if (!toks_uhex || !toks_uhex.length)
          break;
        toks.push(toks_uhex.shift());
        if (!toks_raw || !toks_raw.length)
          break;
        UHex._str2uhexs(toks_raw.shift()).forEach((uhex) => toks.push(uhex));
      }
    } else {
      while (1) {
        if (!toks_raw || !toks_raw.length)
          break;
        UHex._str2uhexs(toks_raw.shift()).forEach((uhex) => toks.push(uhex));
        if (!toks_uhex || !toks_uhex.length)
          break;
        toks.push(toks_uhex.shift());
      }
    }
    return toks;
  }
}
