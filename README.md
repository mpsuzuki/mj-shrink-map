# MJ Shrink-Map Viewer

This is a tool to query the related UCS characters from one UCS character, by the relationship recorded in the MJ Shrink Map (2018).

https://moji.or.jp/wp-content/mojikiban/oscdl/MJShrinkMap.1.2.0.json

The tool is not powerfule to parse MJShrinkMap by itself, it should be precompiled into "mini.json" by other Ruby tools in this repository, like:

```
./parseMJSM.rb ./MJShrinkMap.1.2.0.json ./ivd-20220913.txt ./mji.00602.csv \
  | ./ignoreIVS.rb | ./minify.rb > mini.json
```

When [Keisuke MUTOU](https://github.com/kMUTOU) was working for the Moji_Joho Information Promotion Agency in Japan, he made a good [visualization tool of the relationship of the characters in Moji_Joho glyph set](https://www.jinbun-db.com/journal/pdf/vol_22_29-34.pdf), based on D3.js frontend and SPARQL backend. After the disband of the MJ team under IPA, nobody could carry on with it or migrate to other hosting servers, and it was lost. This is a poorman's substitution of his tool, with no backend server.

# LICENCE

* Following files are provided by the Information Promotion Agency in Japan, licensed under [CC BY-SA 2.1 JP DEED](https://creativecommons.org/licenses/by-sa/2.1/jp/)

    MJShrinkMap.1.2.0.json, MJShrinkMapSchema.1.0.0.json, mji.00602.csv

* Following files are provided by Unicode Consortium, licensed under [UNICODE LICENSE V3](https://www.unicode.org/license.txt)

    ivd-20220913.txt

* Following files are provided by [Taichi KAWABATA](https://github.com/cjkvi/cjkvi-data), licensed under MIT License.

    cjkvi-data/\*

* Following files are provided by GlyphWiki, licensed under [SIL OFL](https://scripts.sil.org/OFL)
    * HanaMinA.ttf, HanaMinB.ttf, HanaMinC.ttf, HanaMinD.ttf, HanaMinE.ttf, HanaMinCmptBMP.ttf, HanaMinCmptSIP.ttf: splitted from Hanazono Mincho A, B.
    * HanaMinFG.ttf: based on Hanazono Mincho B and gw2783629 ver. 20-05-09.

* Following files are originally provided by Kamichi Koichi, licensed under [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
    * JigmoVS_zopfli.woff: based on [Jigmo fonts](https://kamichikoichi.github.io/jigmo/) (Jigmo, Jigmo2, Jigmo3), but their non-ASCII glyphs without UVS are removed.

* Following files are written by mpsuzuki, licensed under GPL v2.

    parseMJSM.rb, ignoreIVS.rb, minify.rb, index.html

# ACKNOWLEDGEMENT

The files written by mpsuzuki are supported by JSPS KAKENHI Grant Number 22K12719.
