#!/usr/bin/env ruby
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "json"
require "set"
require "./ruby-utils/getOpts.rb"

def packRange(arr)

  i = arr.shift()
  ranges = [ [i,i] ]
  while (i = arr.shift())
    if (ranges.last.last + 1 == i)
      ranges.last.pop()
      ranges.last.push(i)
    else
      ranges.push([i, i])
    end
  end

  return ranges.map{|pr|
    if (pr.first == pr.last)
      sprintf("%04X", pr.first)
    else
      sprintf("%04X-%04X", pr.first, pr.last)
    end
  }
end

ojs = Hash.new()
while (STDIN.gets())
  l = $_.chomp().gsub(/#.*/, "")
  next if (l.count(";") < 2)
  ivs, ivd_coll, glyph_name, = l.split(";").map{|t| t.gsub(/^\s*/, "").gsub(/\s*$/, "")}
  base, vs, = ivs.split(/\s+/)
  base_uhex = "U+" + base
  ojs[ivd_coll] = Hash.new() unless (ojs.include?(ivd_coll))
  ojs[ivd_coll][base_uhex] = Array.new() unless (ojs[ivd_coll].include?(base_uhex))
  ojs[ivd_coll][base_uhex] << vs
  # p ojs
end

ojs.keys().sort().each do |ivd_coll|
  ojs[ivd_coll].keys().sort_by{|base_uhex| base_uhex[2..-1].hex()}.each do |base_uhex|
    vss = ojs[ivd_coll][base_uhex].sort().map{|v| v.hex()}
    ojs[ivd_coll][base_uhex] = packRange(vss).join(",")
  end
end

#puts JSON.pretty_generate(ojs)
puts JSON.generate(ojs)
